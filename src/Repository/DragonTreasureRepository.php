<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\DragonTreasure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DragonTreasure>
 *
 * @method DragonTreasure|null find($id, $lockMode = null, $lockVersion = null)
 * @method DragonTreasure|null findOneBy(array $criteria, array $orderBy = null)
 * @method DragonTreasure[]    findAll()
 * @method DragonTreasure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class DragonTreasureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DragonTreasure::class);
    }

    public function save(DragonTreasure $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(DragonTreasure $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
